package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;
	private String current;
	private Flower flower;
	private VisualParameters visualParameters;
	private GrowingTips growingTips;
	private AverageLengthFlower averageLengthFlower;
	private Temperature temperature;
	private Watering watering;
	private final List<Flower> flowerList;

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
		this.flowerList = new ArrayList<>();
	}

	// PLACE YOUR CODE HERE
	public Flowers parse() throws ParserConfigurationException, SAXException, IOException {
		Flowers flowers = new Flowers();
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		parser.parse(new File(xmlFileName), this);
		flowers.setFlowers(flowerList);
		return flowers;
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		current = qName;
		if(XML.FLOWER.equalsTo(current)){
			flower = new Flower();
			return;
		}
		if(XML.VISUAL_PARAMETERS.equalsTo(current)){
			visualParameters = new VisualParameters();
			flower.setVisualParameters(visualParameters);
			return;
		}
		if(XML.GROWING_TIPS.equalsTo(current)){
			growingTips = new GrowingTips();
			flower.setGrowingTips(growingTips);
			return;
		}
		if(XML.AVERAGE_LENGTH_FLOWER.equalsTo(current)){
			averageLengthFlower = new AverageLengthFlower();
			averageLengthFlower.setMeasure(attributes.getValue(XML.MEASURE.value()));
			visualParameters.setAverageLengthFlower(averageLengthFlower);
			return;
		}
		if (XML.TEMPERATURE.equalsTo(current)) {
			temperature = new Temperature();
			temperature.setMeasure(attributes.getValue(XML.MEASURE.value()));
			growingTips.setTemperature(temperature);
			return;
		}
		if (XML.LIGHTING.equalsTo(current)) {
			Lighting lighting = new Lighting();
			lighting.setIsLightRequiring(attributes.getValue(XML.LIGHT_REQUIRING.value()));
			growingTips.setLighting(lighting);
			return;
		}
		if (XML.WATERING.equalsTo(current)) {
			watering = new Watering();
			watering.setMeasure(attributes.getValue(XML.MEASURE.value()));
			growingTips.setWatering(watering);
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		current = null;
		if (XML.FLOWER.equalsTo(qName)) {
			flowerList.add(flower);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) {
		String text = new String(ch, start, length);
		if (current != null) {
			switch (current) {
				case "name":
					flower.setName(text);
					break;
				case "soil":
					flower.setSoil(text);
					break;
				case "origin":
					flower.setOrigin(text);
					break;
				case "multiplying":
					flower.setMultiplying(text);
					break;
				case "stemColour":
					visualParameters.setStemColour(text);
					break;
				case "leafColour":
					visualParameters.setLeafColour(text);
					break;
				case "aveLenFlower":
					averageLengthFlower.setAverageLengthValue(Integer.parseInt(text));
					break;
				case "tempreture":
					temperature.setTemperature(Integer.parseInt(text));
					break;
				case "watering":
					watering.setWaterAmount(Integer.parseInt(text));
					break;
				default:
					break;
			}
		}
	}
}