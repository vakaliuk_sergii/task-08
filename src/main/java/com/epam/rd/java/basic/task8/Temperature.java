package com.epam.rd.java.basic.task8;

public class Temperature {
    private String measure;
    private int degrees;

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getTemperatureValue() { return degrees; }

    public void setTemperature(int degrees) { this.degrees = degrees; }
}
