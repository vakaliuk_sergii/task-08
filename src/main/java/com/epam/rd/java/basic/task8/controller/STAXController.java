package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE
	private Flower flower;
	private VisualParameters visualParameters;
	private GrowingTips growingTips;

	public Flowers parse() throws XMLStreamException {
		Flowers flowers = new Flowers();
		List<Flower> flowerList = new ArrayList<>();
		XMLInputFactory factory =XMLInputFactory.newInstance();
		factory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
		XMLEventReader reader = factory.createXMLEventReader(new StreamSource(xmlFileName));
		while (reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if (event.isCharacters() && event.asCharacters().isWhiteSpace())
				continue;
			String currentElement;
			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				currentElement = startElement.getName().getLocalPart();
				if (XML.FLOWER.equalsTo(currentElement)) {
					flower = new Flower();
					continue;
				}
				if (XML.NAME.equalsTo(currentElement)) {
					event = reader.nextEvent();
					flower.setName(event.asCharacters().getData());
					continue;
				}
				if (XML.SOIL.equalsTo(currentElement)) {
					event = reader.nextEvent();
					flower.setSoil(event.asCharacters().getData());
					continue;
				}
				if (XML.ORIGIN.equalsTo(currentElement)) {
					event = reader.nextEvent();
					flower.setOrigin(event.asCharacters().getData());
					continue;
				}
				if (XML.VISUAL_PARAMETERS.equalsTo(currentElement)) {
					visualParameters = new VisualParameters();
					flower.setVisualParameters(visualParameters);
					continue;
				}
				if (XML.STEM_COLOUR.equalsTo(currentElement)) {
					event = reader.nextEvent();
					visualParameters.setStemColour(event.asCharacters().getData());
					continue;
				}
				if (XML.LEAF_COLOUR.equalsTo(currentElement)) {
					event = reader.nextEvent();
					visualParameters.setLeafColour(event.asCharacters().getData());
					continue;
				}
				if (XML.AVERAGE_LENGTH_FLOWER.equalsTo(currentElement)) {
					AverageLengthFlower averageLengthFlower = new AverageLengthFlower();
					averageLengthFlower.setMeasure(startElement.getAttributeByName(new QName(XML.MEASURE.value())).getValue());
					event = reader.nextEvent();
					averageLengthFlower.setAverageLengthValue(Integer.parseInt(event.asCharacters().getData()));
					visualParameters.setAverageLengthFlower(averageLengthFlower);
					continue;
				}
				if (XML.GROWING_TIPS.equalsTo(currentElement)) {
					growingTips = new GrowingTips();
					flower.setGrowingTips(growingTips);
					continue;
				}
				if (XML.TEMPERATURE.equalsTo(currentElement)) {
					Temperature temperature = new Temperature();
					temperature.setMeasure(startElement.getAttributeByName(new QName(XML.MEASURE.value())).getValue());
					event = reader.nextEvent();
					temperature.setTemperature(Integer.parseInt(event.asCharacters().getData()));
					growingTips.setTemperature(temperature);
					continue;
				}
				if (XML.LIGHTING.equalsTo(currentElement)) {
					Lighting lighting = new Lighting();
					lighting.setIsLightRequiring(startElement.getAttributeByName(new QName(XML.LIGHT_REQUIRING.value())).getValue());
					growingTips.setLighting(lighting);
					continue;
				}
				if (XML.WATERING.equalsTo(currentElement)) {
					Watering watering = new Watering();
					watering.setMeasure(startElement.getAttributeByName(new QName(XML.MEASURE.value())).getValue());
					event = reader.nextEvent();
					watering.setWaterAmount(Integer.parseInt(event.asCharacters().getData()));
					growingTips.setWatering(watering);
					continue;
				}
				if (XML.MULTIPLYING.equalsTo(currentElement)) {
					event = reader.nextEvent();
					flower.setMultiplying(event.asCharacters().getData());
					continue;
				}
			}
			if (event.isEndElement()) {
				EndElement endElement = event.asEndElement();
				currentElement = endElement.getName().getLocalPart();
				if (XML.FLOWER.equalsTo(currentElement)) {
					flowerList.add(flower);
				}
			}
			reader.close();
		}
		flowers.setFlowers(flowerList);
		return flowers;
	}
}