package com.epam.rd.java.basic.task8;

import java.util.ArrayList;
import java.util.List;

public class Flowers {
    private List<Flower> flowers;

    public Flowers() { this.flowers = new ArrayList<>(); }

    public List<Flower> getFlowers() { return flowers; }

    public void setFlowers(List<Flower> flowers) { this.flowers = flowers; }
}
