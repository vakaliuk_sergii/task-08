package com.epam.rd.java.basic.task8;

public class Watering {
    private String measure;
    private int waterAmount;

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getWaterAmount() { return waterAmount; }

    public void setWaterAmount(int waterAmount) { this.waterAmount = waterAmount; }

}
