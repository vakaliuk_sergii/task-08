package com.epam.rd.java.basic.task8.controller;

public enum XML {
    FLOWERS("flowers"), XMLNS("http://www.nure.ua"), XMLNS_XSI("http://www.w3.org/2001/XMLSchema-instance"),
    XSI_SCHEMA_LOCATION("http://www.nure.ua input.xsd "), FLOWER("flower"), NAME("name"), SOIL("soil"),
    ORIGIN("origin"), VISUAL_PARAMETERS("visualParameters"), STEM_COLOUR("stemColour"),
    LEAF_COLOUR("leafColour"), AVERAGE_LENGTH_FLOWER("aveLenFlower"), MEASURE("measure"),
    LIGHT_REQUIRING("lightRequiring"), GROWING_TIPS("growingTips"), TEMPERATURE("tempreture"),
    LIGHTING("lighting"), WATERING("watering"), MULTIPLYING("multiplying");

    private final String value;

    XML(String value) { this.value = value; }

    public boolean equalsTo(String name) { return value.equals(name); }

    public String value() { return value; }

}
