package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.*;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	// PLACE YOUR CODE HERE

	public Flowers parse() throws ParserConfigurationException, IOException, SAXException {
		Flowers flowers = new Flowers();
		List<Flower> flowerList = new ArrayList<>();
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = db.parse(new File(xmlFileName));
		doc.getDocumentElement().normalize();
		NodeList flowerNodes = doc.getElementsByTagName(XML.FLOWER.value());
		for(int i = 0; i < flowerNodes.getLength(); i++) {
			Node node = flowerNodes.item(i);
			Flower flower = new Flower();
			Element element = (Element) node;
			flower.setName(element.getElementsByTagName(XML.NAME.value()).item(0).getTextContent());
			flower.setSoil(element.getElementsByTagName(XML.SOIL.value()).item(0).getTextContent());
			flower.setOrigin(element.getElementsByTagName(XML.ORIGIN.value()).item(0).getTextContent());
			VisualParameters visualParameters = new VisualParameters();
			visualParameters.setStemColour(element.getElementsByTagName(XML.STEM_COLOUR.value()).item(0).getTextContent());
			visualParameters.setLeafColour(element.getElementsByTagName(XML.LEAF_COLOUR.value()).item(0).getTextContent());
			AverageLengthFlower averageLengthFlower = new AverageLengthFlower();
			averageLengthFlower.setMeasure(element.getElementsByTagName(XML.AVERAGE_LENGTH_FLOWER.value()).item(0).getAttributes().item(0).getTextContent());
			averageLengthFlower.setAverageLengthValue(Integer.parseInt(element.getElementsByTagName(XML.AVERAGE_LENGTH_FLOWER.value()).item(0).getTextContent()));
			visualParameters.setAverageLengthFlower(averageLengthFlower);
			flower.setVisualParameters(visualParameters);
			GrowingTips growingTips = new GrowingTips();
			Temperature temperature = new Temperature();
			temperature.setMeasure(element.getElementsByTagName(XML.TEMPERATURE.value()).item(0).getAttributes().item(0).getTextContent());
			temperature.setTemperature(Integer.parseInt(element.getElementsByTagName(XML.TEMPERATURE.value()).item(0).getTextContent()));
			growingTips.setTemperature(temperature);
			Lighting lighting = new Lighting();
			lighting.setIsLightRequiring(element.getElementsByTagName(XML.LIGHTING.value()).item(0).getAttributes().item(0).getTextContent());
			growingTips.setLighting(lighting);
			Watering watering = new Watering();
			watering.setMeasure(element.getElementsByTagName(XML.WATERING.value()).item(0).getAttributes().item(0).getTextContent());
			watering.setWaterAmount(Integer.parseInt(element.getElementsByTagName(XML.WATERING.value()).item(0).getTextContent()));
			growingTips.setWatering(watering);
			flower.setGrowingTips(growingTips);
			flower.setMultiplying(element.getElementsByTagName(XML.MULTIPLYING.value()).item(0).getTextContent());
			flowerList.add(flower);
		}
		flowers.setFlowers(flowerList);
		return flowers;
	}

	public void save(String xmlFileName, Flowers flowers) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document document = db.newDocument();
		Element root = document.createElement(XML.FLOWERS.value());
		root.setAttribute("xmlns", XML.XMLNS.value());
		root.setAttribute("xmlns:xsi", XML.XMLNS_XSI.value());
		root.setAttribute("xsi:schemaLocation", XML.XSI_SCHEMA_LOCATION.value());
		document.appendChild(root);
		List<Flower> flowerList = flowers.getFlowers();
		for (Flower value : flowerList) {
			Element flower = document.createElement(XML.FLOWER.value());
			root.appendChild(flower);
			Element name = document.createElement(XML.NAME.value());
			name.appendChild(document.createTextNode(value.getName()));
			flower.appendChild(name);
			Element soil = document.createElement(XML.SOIL.value());
			soil.appendChild(document.createTextNode(value.getSoil()));
			flower.appendChild(soil);
			Element origin = document.createElement(XML.ORIGIN.value());
			origin.appendChild(document.createTextNode(value.getOrigin()));
			flower.appendChild(origin);
			Element visualParameters = document.createElement(XML.VISUAL_PARAMETERS.value());
			Element stemColour = document.createElement(XML.STEM_COLOUR.value());
			stemColour.appendChild(document.createTextNode(value.getVisualParameters().getStemColour()));
			visualParameters.appendChild(stemColour);
			Element leafColour = document.createElement(XML.LEAF_COLOUR.value());
			leafColour.appendChild(document.createTextNode(value.getVisualParameters().getLeafColour()));
			visualParameters.appendChild(leafColour);
			Element averageLengthFlower = document.createElement(XML.AVERAGE_LENGTH_FLOWER.value());
			averageLengthFlower.setAttribute(XML.MEASURE.value(), value.getVisualParameters().getAverageLengthFlower().getMeasure());
			averageLengthFlower.appendChild(document.createTextNode(String.valueOf(value.getVisualParameters().getAverageLengthFlower().getAverageLengthValue())));
			visualParameters.appendChild(averageLengthFlower);
			flower.appendChild(visualParameters);
			Element growingTips = document.createElement(XML.GROWING_TIPS.value());
			Element temperature = document.createElement(XML.TEMPERATURE.value());
			temperature.setAttribute(XML.MEASURE.value(), value.getGrowingTips().getTemperature().getMeasure());
			temperature.appendChild(document.createTextNode(String.valueOf(value.getGrowingTips().getTemperature().getTemperatureValue())));
			growingTips.appendChild(temperature);
			Element lighting = document.createElement(XML.LIGHTING.value());
			lighting.setAttribute(XML.LIGHT_REQUIRING.value(), value.getGrowingTips().getLighting().getIsLightRequiring());
			growingTips.appendChild(lighting);
			Element watering = document.createElement(XML.WATERING.value());
			watering.setAttribute(XML.MEASURE.value(), value.getGrowingTips().getWatering().getMeasure());
			watering.appendChild(document.createTextNode(String.valueOf(value.getGrowingTips().getWatering().getWaterAmount())));
			growingTips.appendChild(watering);
			flower.appendChild(growingTips);
			Element multiplyingElement = document.createElement(XML.MULTIPLYING.value());
			multiplyingElement.appendChild(document.createTextNode(value.getMultiplying()));
			flower.appendChild(multiplyingElement);
		}
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		DOMSource domSource = new DOMSource(document);
		StreamResult streamResult = new StreamResult(new File(xmlFileName));
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.transform(domSource, streamResult);
	}

}
