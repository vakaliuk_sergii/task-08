package com.epam.rd.java.basic.task8;

public class AverageLengthFlower {
    private String measure;
    private int averageLengthValue;

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    public int getAverageLengthValue() { return averageLengthValue; }

    public void setAverageLengthValue(int averageLengthValue) { this.averageLengthValue = averageLengthValue; }
}
