package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		boolean isValid = validate(xmlFileName);
		if (!isValid) { return; }
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersDOM = domController.parse();
		// sort (case 1)
		// PLACE YOUR CODE HERE
		sort(flowersDOM.getFlowers(), Flower::getMultiplying);
		// save
		String outputXmlFile = "output.dom.xml";
		// PLACE YOUR CODE HERE
		domController.save(outputXmlFile, flowersDOM);
		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////
		
		// get
		SAXController saxController = new SAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersSAX = saxController.parse();
		// sort  (case 2)
		// PLACE YOUR CODE HERE
		sort(flowersDOM.getFlowers(), Flower::getOrigin);
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		domController.save(outputXmlFile, flowersSAX);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		// PLACE YOUR CODE HERE
		Flowers flowersSTAX = staxController.parse();
		// sort  (case 3)
		// PLACE YOUR CODE HERE
		sort(flowersSTAX.getFlowers(), Flower::getName);
		// save
		outputXmlFile = "output.stax.xml";
		// PLACE YOUR CODE HERE
		domController.save(outputXmlFile, flowersSTAX);
	}

	private static <T, U extends Comparable<? super U>> void sort(List<T> items, Function<T, U> extractor) {
		items.sort(Comparator.comparing(extractor));
	}

	private static boolean validate (String xmlFileName) throws SAXException, IOException {
		SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Source source = new StreamSource(new File("input.xsd"));
		Schema schema = factory.newSchema(source);
		Validator validator = schema.newValidator();
		validator.validate(new StreamSource(new File(xmlFileName)));
		return true;
	}

}
