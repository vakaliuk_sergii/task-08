package com.epam.rd.java.basic.task8;

public class VisualParameters {
    private String stemColour;
    private String leafColour;
    private AverageLengthFlower averageLengthFlower;

    public String getStemColour() {
        return stemColour;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public AverageLengthFlower getAverageLengthFlower() {
        return averageLengthFlower;
    }

    public void setAverageLengthFlower(AverageLengthFlower averageLengthFlower) { this.averageLengthFlower = averageLengthFlower; }

}
